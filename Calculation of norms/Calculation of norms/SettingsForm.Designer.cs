﻿
namespace Calculation_of_norms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.calculatedTheNorm = new System.Windows.Forms.TextBox();
            this.checkedTheNorm = new System.Windows.Forms.TextBox();
            this.toolWidth = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.workshopTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.saveTo_textBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Рассчитал норму:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Проверил норму:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Цех:";
            // 
            // calculatedTheNorm
            // 
            this.calculatedTheNorm.Location = new System.Drawing.Point(176, 13);
            this.calculatedTheNorm.Name = "calculatedTheNorm";
            this.calculatedTheNorm.Size = new System.Drawing.Size(143, 22);
            this.calculatedTheNorm.TabIndex = 3;
            // 
            // checkedTheNorm
            // 
            this.checkedTheNorm.Location = new System.Drawing.Point(176, 41);
            this.checkedTheNorm.Name = "checkedTheNorm";
            this.checkedTheNorm.Size = new System.Drawing.Size(143, 22);
            this.checkedTheNorm.TabIndex = 4;
            // 
            // toolWidth
            // 
            this.toolWidth.Location = new System.Drawing.Point(176, 97);
            this.toolWidth.Name = "toolWidth";
            this.toolWidth.Size = new System.Drawing.Size(143, 22);
            this.toolWidth.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(176, 166);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 41);
            this.button1.TabIndex = 6;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // workshopTextBox
            // 
            this.workshopTextBox.Location = new System.Drawing.Point(176, 69);
            this.workshopTextBox.Name = "workshopTextBox";
            this.workshopTextBox.Size = new System.Drawing.Size(143, 22);
            this.workshopTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ширина пропила (мм):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Путь для сохранения:";
            // 
            // saveTo_textBox
            // 
            this.saveTo_textBox.Location = new System.Drawing.Point(176, 125);
            this.saveTo_textBox.Name = "saveTo_textBox";
            this.saveTo_textBox.Size = new System.Drawing.Size(245, 22);
            this.saveTo_textBox.TabIndex = 10;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(427, 125);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 219);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.saveTo_textBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.workshopTextBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.toolWidth);
            this.Controls.Add(this.checkedTheNorm);
            this.Controls.Add(this.calculatedTheNorm);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Настройки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox calculatedTheNorm;
        private System.Windows.Forms.TextBox checkedTheNorm;
        private System.Windows.Forms.TextBox toolWidth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox workshopTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox saveTo_textBox;
        private System.Windows.Forms.Button button2;
    }
}