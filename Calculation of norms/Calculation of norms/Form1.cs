﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using System.IO;

namespace Calculation_of_norms
{
    public partial class Form1 : Form
    {
        private int i;
        //для расчетов
        private float remains = 1;//остаток
        private int suppliedLength;//поставляемая длина
        private float theWidthOfTheCut;//ширина пропила
        private float blankLength;//длина заготовки
        private int numberOfBlanks; //количество заготовок
        private float conditionalLengthTakingIntoAccountLosses; //условная длина с учетом потерь
        private float metalConsumptionRate;//норма расхода металла

        private string[] arrayString;
        BindingSource[] bind;

        public Form1()
        {
            InitializeComponent();
            bind = new BindingSource[] { bindingSource1, bindingSource2, bindingSource3 };
            Fill_combo();
        }

        /// <summary>
        /// Метод для получение имен всех таблиц из БД
        /// </summary>
        private void Fill_combo()
        {
            GOST_tables.Items.Clear();
            OleDbConnection dbCon = new OleDbConnection(
             @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=DataBaseGOST.mdb");
            dbCon.Open();
            System.Data.DataTable tbls = dbCon.GetSchema("Tables", new string[] { null, null, null, "TABLE" }); //список всех таблиц
            foreach (DataRow row in tbls.Rows)
            {
                string TableName = row["TABLE_NAME"].ToString();
                GOST_tables.Items.Add(TableName);
            }
            dbCon.Close();
            GOST_tables.SelectedIndex = 0;
        }

        /// <summary>
        /// Расчет нормы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            saveFromExcel_Button.Enabled = true;//активация кнопки "сохранить"
            Calculation();
        }

        private void Calculation()
        {
            CalculationClean();//чистим переменные

            foreach (char ch in partWeight_textBox.Text)//обработка исключений
            {
                if (char.IsLetter(ch) || char.IsWhiteSpace(ch))
                {
                    MessageBox.Show("Не указан размер по чертежу или введены некорректные данные", "Ошибка!");
                    return;
                }
            }
            string s = partWeight_textBox.Text;
            var partWeight = s.Replace('.', ',');
            suppliedLength = Convert.ToInt32(length_TextBox.Text);//переводим длину из формата текст в интеджир
            //получаем ширину пропила из файла с настройками
            StreamReader settingsFile = new StreamReader("Setting.txt");
            arrayString = File.ReadAllLines("Setting.txt");
            settingsFile.Close();
            theWidthOfTheCut = float.Parse(arrayString[3]);
            //Для получения длины заготовки складываем длину по чертежу и ширину пропила (1)
            blankLength = theWidthOfTheCut + float.Parse(partWeight);

            while (remains >= 0)//цикл где считаем кол-во деталей и получаем остаток (2)
            {
                numberOfBlanks++;
                remains = suppliedLength - (blankLength * numberOfBlanks);
                if (remains < blankLength)
                {
                    break;
                }

            }
            //Расчет условной длины с учетом потерь (3)
            conditionalLengthTakingIntoAccountLosses = blankLength + (remains / numberOfBlanks);
            /*crutch
             *так-как в базу данных теоретический вес заводился через точку
             *при переводе из string во float происходит ошибка
             *ниже выполняется замена символа методом String.Replace()
             */
            s = theoreticalWeight_TextBox.Text;
            var news = s.Replace('.', ',');

            //Рачет нормы (4)
            metalConsumptionRate = blankLength * float.Parse(news) / 1000;
            //Вывод
            DataOutput();
        }

        /// <summary>
        /// Работа с внешним файлом Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            //обновление данных
            StreamReader settingsFile = new StreamReader("Setting.txt");
            arrayString = File.ReadAllLines("Setting.txt");
            settingsFile.Close();


            String sndc = Path.Combine(Directory.GetCurrentDirectory(), "Form.xls");//получаем полный путь к файлу
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;

            Excel excel = new Excel(sndc, 1);//открываем книгу на первом листе

            //Передаем данные из приложения в excel файл
            excel.WriteToCell(1, 26, loginField.Text);//номер детали
            excel.WriteToCell(5, 8, textBox1.Text);//наименование детали
            excel.WriteToCell(1, 5, nomenklatureNumber_TextBox.Text);//номенклатурный номер
            excel.WriteToCell(5, 0, arrayString[2]);//цех
            excel.WriteToCell(11, 0, sizeTables_ComboBox.Text);//размер по ГОСТ
            excel.WriteToCell(11, 7, steelGrade_TextBox.Text);//Марка стали
            excel.WriteToCell(7, 25, Convert.ToString(GOST_tables.Items[i]));//сортамент
            excel.WriteToCell(9, 25, gostForSteel_TextBox.Text);//ГОСТ на сталь
            excel.WriteToCell(10, 25, technicalRequirements_TextBox.Text);//технические требования по ГОСТ
            excel.WriteToCell(12, 28, theoreticalWeight_TextBox.Text);//теоритический вес
            excel.WriteToCell(16, 0, partWeight_textBox.Text);//размер по чертежу
            excel.WriteToCell(16, 3, blankLength_textBox.Text);//длина заготовки
            excel.WriteToCell(16, 11, length_TextBox.Text); //размер проката
            excel.WriteToCell(16, 15, numberOfBlanks_textBox.Text);//кол-во деталей
            excel.WriteToCell(16, 16, remains_textBox.Text);//остаток
            excel.WriteToCell(16, 20, Convert.ToString(theWidthOfTheCut));//ширина пропила
            excel.WriteToCell(16, 29, conditionalLengthTakingIntoAccountLosses_textBox.Text);//услю дл. с уч. потерь
            excel.WriteToCell(20,1, textBox9.Text);//чистый вес
            excel.WriteToCell(20, 14, metalConsumptionRate_textBox.Text);//норма расхода
            excel.WriteToCell(26, 0, arrayString[0]);//разработал
            excel.WriteToCell(26, 6, arrayString[1]);//проверил

            //Проверка создания пути для сохранения
            if(arrayString[4] == "Укажите путь для сохранения")
            {
                MessageBox.Show("Не указан путь для сохранения", "Ошибка!");
                this.Cursor = System.Windows.Forms.Cursors.Default;
                return;
            }

            if(loginField.Text == "")
            {
                MessageBox.Show("Не указан номер детали", "Ошибка!");
                this.Cursor = System.Windows.Forms.Cursors.Default;
                return;
            }
            excel.SaveAs(arrayString[4] + "\\" + loginField.Text + ".xls");
            excel.Close();
            this.Cursor = System.Windows.Forms.Cursors.Default;
            MessageBox.Show("Файл успешно сохранен");
        }

        private void НастройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm settingsForm = new SettingsForm();
            this.Enabled = false;
            settingsForm.ShowDialog();
            this.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dataBaseGOSTDataSet._9941_81". При необходимости она может быть перемещена или удалена.
            this._9941_81TableAdapter.Fill(this.dataBaseGOSTDataSet._9941_81);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dataBaseGOSTDataSet._8734_75". При необходимости она может быть перемещена или удалена.
            this._8734_75TableAdapter.Fill(this.dataBaseGOSTDataSet._8734_75);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dataBaseGOSTDataSet._2590_2006". При необходимости она может быть перемещена или удалена.
            this._2590_2006TableAdapter.Fill(this.dataBaseGOSTDataSet._2590_2006);

        }

        private void GOST_tables_SelectedIndexChanged(object sender, EventArgs e)
        {
            //создаем связь comboBox'ов, что-бы при выборе госта подгружались все данные из этой таблицы
            CleanDataBinding();
            i = GOST_tables.SelectedIndex;
            sizeTables_ComboBox.DataSource = bind[i];
            nomenklatureNumber_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "Номенклатурный", true, DataSourceUpdateMode.OnValidation));
            steelGrade_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "Марка", true, DataSourceUpdateMode.OnValidation));
            technicalRequirements_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "техтребования", true, DataSourceUpdateMode.OnValidation));
            theoreticalWeight_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "Масса 1м, кг", true, DataSourceUpdateMode.OnValidation));
            assortment_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "Сортамент", true, DataSourceUpdateMode.OnValidation));
            length_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "Длина", true, DataSourceUpdateMode.OnValidation));
            gostForSteel_TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", bind[i], "Маркистали", true, DataSourceUpdateMode.OnValidation));
        }
        
        private void CleanDataBinding()
        {
            //чистим формы, для исключения ошибок 
            nomenklatureNumber_TextBox.DataBindings.Clear();
            steelGrade_TextBox.DataBindings.Clear();
            technicalRequirements_TextBox.DataBindings.Clear();
            theoreticalWeight_TextBox.DataBindings.Clear();
            assortment_TextBox.DataBindings.Clear();
            length_TextBox.DataBindings.Clear();
            gostForSteel_TextBox.DataBindings.Clear();
        }

        private void DataOutput()
        {
            blankLength_textBox.Text = Convert.ToString(blankLength);
            numberOfBlanks_textBox.Text = Convert.ToString(numberOfBlanks);
            remains_textBox.Text = Convert.ToString(remains);
            conditionalLengthTakingIntoAccountLosses_textBox.Text = Convert.ToString(conditionalLengthTakingIntoAccountLosses);
            metalConsumptionRate_textBox.Text = Convert.ToString(metalConsumptionRate);
        }

        /// <summary>
        /// Метод для очистки переменных перед каждым расчетом
        /// </summary>
        private void CalculationClean()
        {
            suppliedLength = 0;
            blankLength = 0;
            remains = 1;
            numberOfBlanks = 0;
        }
    }
}
