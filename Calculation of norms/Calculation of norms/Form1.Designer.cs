﻿
namespace Calculation_of_norms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.loginField = new System.Windows.Forms.TextBox();
            this.label_nomer_detali = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GOST_tables = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.sizeTables_ComboBox = new System.Windows.Forms.ComboBox();
            this.bindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.dataBaseGOSTDataSet = new Calculation_of_norms.DataBaseGOSTDataSet();
            this.calculateButton = new System.Windows.Forms.Button();
            this.nomenklatureNumber_TextBox = new System.Windows.Forms.TextBox();
            this.steelGrade_TextBox = new System.Windows.Forms.TextBox();
            this.technicalRequirements_TextBox = new System.Windows.Forms.TextBox();
            this.theoreticalWeight_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.assortment_TextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.gostForSteel_TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.length_TextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.saveFromExcel_Button = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.partWeight_textBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this._2590_2006TableAdapter = new Calculation_of_norms.DataBaseGOSTDataSetTableAdapters._2590_2006TableAdapter();
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this._8734_75TableAdapter = new Calculation_of_norms.DataBaseGOSTDataSetTableAdapters._8734_75TableAdapter();
            this._9941_81TableAdapter = new Calculation_of_norms.DataBaseGOSTDataSetTableAdapters._9941_81TableAdapter();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.metalConsumptionRate_textBox = new System.Windows.Forms.TextBox();
            this.conditionalLengthTakingIntoAccountLosses_textBox = new System.Windows.Forms.TextBox();
            this.remains_textBox = new System.Windows.Forms.TextBox();
            this.numberOfBlanks_textBox = new System.Windows.Forms.TextBox();
            this.blankLength_textBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseGOSTDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginField
            // 
            this.loginField.Location = new System.Drawing.Point(128, 37);
            this.loginField.Name = "loginField";
            this.loginField.Size = new System.Drawing.Size(151, 22);
            this.loginField.TabIndex = 0;
            // 
            // label_nomer_detali
            // 
            this.label_nomer_detali.AutoSize = true;
            this.label_nomer_detali.Location = new System.Drawing.Point(6, 40);
            this.label_nomer_detali.Name = "label_nomer_detali";
            this.label_nomer_detali.Size = new System.Drawing.Size(106, 17);
            this.label_nomer_detali.TabIndex = 1;
            this.label_nomer_detali.Text = "Номер детали:";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(6, 68);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(110, 17);
            this.label_name.TabIndex = 2;
            this.label_name.Text = "Наименование:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 65);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(151, 22);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "ГОСТ:";
            // 
            // GOST_tables
            // 
            this.GOST_tables.FormattingEnabled = true;
            this.GOST_tables.Location = new System.Drawing.Point(128, 93);
            this.GOST_tables.Name = "GOST_tables";
            this.GOST_tables.Size = new System.Drawing.Size(151, 24);
            this.GOST_tables.TabIndex = 2;
            this.GOST_tables.SelectedIndexChanged += new System.EventHandler(this.GOST_tables_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Размер:";
            // 
            // sizeTables_ComboBox
            // 
            this.sizeTables_ComboBox.DataSource = this.bindingSource3;
            this.sizeTables_ComboBox.DisplayMember = "Размер";
            this.sizeTables_ComboBox.FormattingEnabled = true;
            this.sizeTables_ComboBox.Location = new System.Drawing.Point(128, 123);
            this.sizeTables_ComboBox.Name = "sizeTables_ComboBox";
            this.sizeTables_ComboBox.Size = new System.Drawing.Size(151, 24);
            this.sizeTables_ComboBox.TabIndex = 3;
            // 
            // bindingSource3
            // 
            this.bindingSource3.DataMember = "9941-81";
            this.bindingSource3.DataSource = this.dataBaseGOSTDataSet;
            // 
            // dataBaseGOSTDataSet
            // 
            this.dataBaseGOSTDataSet.DataSetName = "DataBaseGOSTDataSet";
            this.dataBaseGOSTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(75, 138);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(138, 40);
            this.calculateButton.TabIndex = 6;
            this.calculateButton.Text = "Рассчитать норму";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // nomenklatureNumber_TextBox
            // 
            this.nomenklatureNumber_TextBox.Location = new System.Drawing.Point(191, 37);
            this.nomenklatureNumber_TextBox.Name = "nomenklatureNumber_TextBox";
            this.nomenklatureNumber_TextBox.ReadOnly = true;
            this.nomenklatureNumber_TextBox.Size = new System.Drawing.Size(151, 22);
            this.nomenklatureNumber_TextBox.TabIndex = 10;
            // 
            // steelGrade_TextBox
            // 
            this.steelGrade_TextBox.Location = new System.Drawing.Point(191, 68);
            this.steelGrade_TextBox.Name = "steelGrade_TextBox";
            this.steelGrade_TextBox.ReadOnly = true;
            this.steelGrade_TextBox.Size = new System.Drawing.Size(151, 22);
            this.steelGrade_TextBox.TabIndex = 11;
            // 
            // technicalRequirements_TextBox
            // 
            this.technicalRequirements_TextBox.Location = new System.Drawing.Point(191, 184);
            this.technicalRequirements_TextBox.Name = "technicalRequirements_TextBox";
            this.technicalRequirements_TextBox.ReadOnly = true;
            this.technicalRequirements_TextBox.Size = new System.Drawing.Size(151, 22);
            this.technicalRequirements_TextBox.TabIndex = 12;
            // 
            // theoreticalWeight_TextBox
            // 
            this.theoreticalWeight_TextBox.Location = new System.Drawing.Point(191, 96);
            this.theoreticalWeight_TextBox.Name = "theoreticalWeight_TextBox";
            this.theoreticalWeight_TextBox.ReadOnly = true;
            this.theoreticalWeight_TextBox.Size = new System.Drawing.Size(151, 22);
            this.theoreticalWeight_TextBox.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Номенклатурный номер:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "Марка материала:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(179, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Технические требования:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Теоретический вес:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Сортамент:";
            // 
            // assortment_TextBox
            // 
            this.assortment_TextBox.Location = new System.Drawing.Point(191, 156);
            this.assortment_TextBox.Name = "assortment_TextBox";
            this.assortment_TextBox.ReadOnly = true;
            this.assortment_TextBox.Size = new System.Drawing.Size(151, 22);
            this.assortment_TextBox.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.gostForSteel_TextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.length_TextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.assortment_TextBox);
            this.groupBox1.Controls.Add(this.nomenklatureNumber_TextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.steelGrade_TextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.technicalRequirements_TextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.theoreticalWeight_TextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(314, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 240);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Табличные данные:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 215);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 17);
            this.label11.TabIndex = 23;
            this.label11.Text = "ГОСТ на материал:";
            // 
            // gostForSteel_TextBox
            // 
            this.gostForSteel_TextBox.Location = new System.Drawing.Point(191, 212);
            this.gostForSteel_TextBox.Name = "gostForSteel_TextBox";
            this.gostForSteel_TextBox.ReadOnly = true;
            this.gostForSteel_TextBox.Size = new System.Drawing.Size(151, 22);
            this.gostForSteel_TextBox.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Размер проката:";
            // 
            // length_TextBox
            // 
            this.length_TextBox.Location = new System.Drawing.Point(191, 124);
            this.length_TextBox.Name = "length_TextBox";
            this.length_TextBox.ReadOnly = true;
            this.length_TextBox.Size = new System.Drawing.Size(151, 22);
            this.length_TextBox.TabIndex = 20;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.sizeTables_ComboBox);
            this.groupBox2.Controls.Add(this.loginField);
            this.groupBox2.Controls.Add(this.label_nomer_detali);
            this.groupBox2.Controls.Add(this.label_name);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.GOST_tables);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 185);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Основные данные детали:";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox3.Controls.Add(this.saveFromExcel_Button);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.calculateButton);
            this.groupBox3.Controls.Add(this.partWeight_textBox);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(12, 214);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 248);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Данные для расчета нормы:";
            // 
            // saveFromExcel_Button
            // 
            this.saveFromExcel_Button.Enabled = false;
            this.saveFromExcel_Button.Location = new System.Drawing.Point(75, 184);
            this.saveFromExcel_Button.Name = "saveFromExcel_Button";
            this.saveFromExcel_Button.Size = new System.Drawing.Size(138, 40);
            this.saveFromExcel_Button.TabIndex = 7;
            this.saveFromExcel_Button.Text = "Сохранить";
            this.saveFromExcel_Button.UseVisualStyleBackColor = true;
            this.saveFromExcel_Button.Click += new System.EventHandler(this.button2_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 17);
            this.label10.TabIndex = 13;
            this.label10.Text = "Масса детали:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(152, 65);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(127, 22);
            this.textBox9.TabIndex = 5;
            // 
            // partWeight_textBox
            // 
            this.partWeight_textBox.Location = new System.Drawing.Point(152, 37);
            this.partWeight_textBox.Name = "partWeight_textBox";
            this.partWeight_textBox.Size = new System.Drawing.Size(127, 22);
            this.partWeight_textBox.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "Размер по чертежу:";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "2590-2006";
            this.bindingSource1.DataSource = this.dataBaseGOSTDataSet;
            // 
            // _2590_2006TableAdapter
            // 
            this._2590_2006TableAdapter.ClearBeforeFill = true;
            // 
            // bindingSource2
            // 
            this.bindingSource2.DataMember = "8734-75";
            this.bindingSource2.DataSource = this.dataBaseGOSTDataSet;
            // 
            // _8734_75TableAdapter
            // 
            this._8734_75TableAdapter.ClearBeforeFill = true;
            // 
            // _9941_81TableAdapter
            // 
            this._9941_81TableAdapter.ClearBeforeFill = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(697, 28);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(98, 24);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.НастройкиToolStripMenuItem_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox4.Controls.Add(this.metalConsumptionRate_textBox);
            this.groupBox4.Controls.Add(this.conditionalLengthTakingIntoAccountLosses_textBox);
            this.groupBox4.Controls.Add(this.remains_textBox);
            this.groupBox4.Controls.Add(this.numberOfBlanks_textBox);
            this.groupBox4.Controls.Add(this.blankLength_textBox);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Location = new System.Drawing.Point(314, 282);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(371, 180);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Результат:";
            // 
            // metalConsumptionRate_textBox
            // 
            this.metalConsumptionRate_textBox.Location = new System.Drawing.Point(191, 142);
            this.metalConsumptionRate_textBox.Name = "metalConsumptionRate_textBox";
            this.metalConsumptionRate_textBox.ReadOnly = true;
            this.metalConsumptionRate_textBox.Size = new System.Drawing.Size(100, 22);
            this.metalConsumptionRate_textBox.TabIndex = 9;
            // 
            // conditionalLengthTakingIntoAccountLosses_textBox
            // 
            this.conditionalLengthTakingIntoAccountLosses_textBox.Location = new System.Drawing.Point(191, 114);
            this.conditionalLengthTakingIntoAccountLosses_textBox.Name = "conditionalLengthTakingIntoAccountLosses_textBox";
            this.conditionalLengthTakingIntoAccountLosses_textBox.ReadOnly = true;
            this.conditionalLengthTakingIntoAccountLosses_textBox.Size = new System.Drawing.Size(100, 22);
            this.conditionalLengthTakingIntoAccountLosses_textBox.TabIndex = 8;
            // 
            // remains_textBox
            // 
            this.remains_textBox.Location = new System.Drawing.Point(191, 86);
            this.remains_textBox.Name = "remains_textBox";
            this.remains_textBox.ReadOnly = true;
            this.remains_textBox.Size = new System.Drawing.Size(100, 22);
            this.remains_textBox.TabIndex = 7;
            // 
            // numberOfBlanks_textBox
            // 
            this.numberOfBlanks_textBox.Location = new System.Drawing.Point(191, 58);
            this.numberOfBlanks_textBox.Name = "numberOfBlanks_textBox";
            this.numberOfBlanks_textBox.ReadOnly = true;
            this.numberOfBlanks_textBox.Size = new System.Drawing.Size(100, 22);
            this.numberOfBlanks_textBox.TabIndex = 6;
            // 
            // blankLength_textBox
            // 
            this.blankLength_textBox.Location = new System.Drawing.Point(191, 30);
            this.blankLength_textBox.Name = "blankLength_textBox";
            this.blankLength_textBox.ReadOnly = true;
            this.blankLength_textBox.Size = new System.Drawing.Size(100, 22);
            this.blankLength_textBox.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 145);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "Норма расхода:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(148, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "Усл. дл. с уч. потерь:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Остаток материала:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 17);
            this.label13.TabIndex = 1;
            this.label13.Text = "Количество деталей:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Длина заготовки:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 467);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Расчет норм основного материала";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseGOSTDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox loginField;
        private System.Windows.Forms.Label label_nomer_detali;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox GOST_tables;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox sizeTables_ComboBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.TextBox nomenklatureNumber_TextBox;
        private System.Windows.Forms.TextBox steelGrade_TextBox;
        private System.Windows.Forms.TextBox technicalRequirements_TextBox;
        private System.Windows.Forms.TextBox theoreticalWeight_TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox assortment_TextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox partWeight_textBox;
        private System.Windows.Forms.Label label8;
        private DataBaseGOSTDataSet dataBaseGOSTDataSet;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DataBaseGOSTDataSetTableAdapters._2590_2006TableAdapter _2590_2006TableAdapter;
        private System.Windows.Forms.BindingSource bindingSource2;
        private DataBaseGOSTDataSetTableAdapters._8734_75TableAdapter _8734_75TableAdapter;
        private System.Windows.Forms.BindingSource bindingSource3;
        private DataBaseGOSTDataSetTableAdapters._9941_81TableAdapter _9941_81TableAdapter;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.Button saveFromExcel_Button;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox length_TextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox gostForSteel_TextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox metalConsumptionRate_textBox;
        private System.Windows.Forms.TextBox conditionalLengthTakingIntoAccountLosses_textBox;
        private System.Windows.Forms.TextBox remains_textBox;
        private System.Windows.Forms.TextBox numberOfBlanks_textBox;
        private System.Windows.Forms.TextBox blankLength_textBox;
        private System.Windows.Forms.Label label16;
    }
}

