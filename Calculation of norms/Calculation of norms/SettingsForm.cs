﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Calculation_of_norms
{
    public partial class SettingsForm : Form
    {
        //создаем массив со строковыми данными
        private string[] arrayString;
        public SettingsForm()
        {
            InitializeComponent();
            FillSettingsFromFile();
        }
        /// <summary>
        /// Метод для сохранение новых данных в файле настроек приложения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            StreamReader settingsFile = new StreamReader("Setting.txt");
            arrayString[0] = Convert.ToString(calculatedTheNorm.Text);
            arrayString[1] = Convert.ToString(checkedTheNorm.Text);
            arrayString[2] = Convert.ToString(workshopTextBox.Text);
            arrayString[3] = Convert.ToString(toolWidth.Text);
            settingsFile.Close();
            File.WriteAllLines("Setting.txt", arrayString);
            this.Close();

        }

        private void FillSettingsFromFile()
        {
            /*создаем новый экземпляр класса
            *заполняем массив строками
            *заполняем textBox из массива 
            *закрываем поток
            **/
            StreamReader settingsFile = new StreamReader("Setting.txt");
            arrayString = File.ReadAllLines("Setting.txt");
            calculatedTheNorm.Text = arrayString[0];
            checkedTheNorm.Text = arrayString[1];
            workshopTextBox.Text = arrayString[2];
            toolWidth.Text = arrayString[3];
            saveTo_textBox.Text = arrayString[4];
            settingsFile.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                saveTo_textBox.Text = FBD.SelectedPath;
                arrayString[4] = saveTo_textBox.Text;
                File.WriteAllLines("Setting.txt", arrayString);
            }
        }
    }
}
